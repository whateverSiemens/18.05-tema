#include "spring\Application\MonoInput32.h"
#include <qendian.h>
#include <iostream>

MonoInput32::MonoInput32(double timeSlice, unsigned sampleRate) :MonoInput(timeSlice, sampleRate)
{
	audioFormat.setSampleSize(32);
	audioFormat.setSampleType(QAudioFormat::SignedInt);
}

MonoInput32::~MonoInput32()
{
}

qint64 MonoInput32::writeData(const char *data, qint64 len)
{
	//const auto *ptr = reinterpret_cast<const unsigned char *>(data);
	const auto *ptr = data;

	for (auto i = 0; i < len / channelBytes; ++i) {

		qint32 value = 0;

		value = qFromLittleEndian<qint32>(ptr);

		auto level = float(value) * (5. / maxAmplitude);
		timeData.push_back(level);
		ptr += channelBytes;
	}

	if (timeData.size() > dataLength)
		timeData.remove(0, timeData.size() - dataLength);

	std::cout << "data received " << len << std::endl;

	return len;
}