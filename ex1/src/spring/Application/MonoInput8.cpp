#include "spring\Application\MonoInput8.h"
#include <qendian.h>
#include <iostream>

MonoInput8::MonoInput8(double timeSlice, unsigned sampleRate):MonoInput(timeSlice,sampleRate)
{
	audioFormat.setSampleSize(8);
	audioFormat.setSampleType(QAudioFormat::UnSignedInt);
	
	channelBytes = 1;

	maxAmplitude = 32767 * 2 + 1;
}

MonoInput8::~MonoInput8()
{
}

qint64 MonoInput8::writeData(const char *data, qint64 len)
{
	const auto *ptr = reinterpret_cast<const unsigned char *>(data);
	//const auto *ptr = data;

	for (auto i = 0; i < len / channelBytes; ++i) {

		qint32 value = 0;

		value = qFromLittleEndian<qint8>(ptr);

		auto level = float(value) * (5. / maxAmplitude);
		timeData.push_back(level);
		ptr += channelBytes;
	}

	if (timeData.size() > dataLength)
		timeData.remove(0, timeData.size() - dataLength);

	std::cout << "data received " << len << std::endl;

	return len;
}