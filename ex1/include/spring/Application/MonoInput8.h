#pragma once
#include "spring\Application\MonoInput.h"

class MonoInput8 : public MonoInput
{
public:
	MonoInput8(double timeSlice, unsigned sampleRate);
	virtual ~MonoInput8();

	qint64 writeData(const char *data, qint64 len) override;

};

