#pragma once
#include "spring\Application\MonoInput.h"

class MonoInput32 : public MonoInput
{
public:
	MonoInput32(double timeSlice, unsigned sampleRate);
	virtual ~MonoInput32();

	qint64 writeData(const char *data, qint64 len) override;

};

